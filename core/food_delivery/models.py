from django.db import models


class Restaurant(models.Model):
    name = models.CharField(max_length=255)
    location = models.TextField()
    rating = models.FloatField()

    def __str__(self):
        return self.name
