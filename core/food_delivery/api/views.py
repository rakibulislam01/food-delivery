from rest_framework import generics, authentication, permissions, status, response
from ..serializers import RestaurantSerializer
from ..models import Restaurant


class RestaurantListAPIView(generics.ListAPIView):
    """
    List all Restaurant.

    * Requires token authentication.
    * Only authorize are able to access this view.
    """
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAdminUser]
    queryset = Restaurant
    serializer_class = RestaurantSerializer

    def get_queryset(self):
        q = self.request.GET.get('q')
        if q:
            return Restaurant.objects.filter(name=q).order_by('-id')
        query_list = Restaurant.objects.all().order_by('-id')
        return query_list
