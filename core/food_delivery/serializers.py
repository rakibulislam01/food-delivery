from django.contrib.auth import get_user_model, authenticate
from rest_framework import serializers
from .models import Restaurant


class RestaurantSerializer(serializers.ModelSerializer):
    """Serializer for the restaurant object"""

    class Meta:
        model = Restaurant
        fields = ('name', 'location', 'rating')
