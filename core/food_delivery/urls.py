from django.urls import path
from .api.views import RestaurantListAPIView
from .views import LoginRegView, HomeView, password_reset

app_name = 'food_delivery'

urlpatterns = [
    path('api/restaurant-list/', RestaurantListAPIView.as_view(), name='restaurant_list'),
    path('', HomeView.as_view(), name='home'),
    path('login/', LoginRegView.as_view(), name='login'),
    path('password-reset/', password_reset, name='password_reset')
]
