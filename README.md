# Food delivery APP

## Task List
* Food delivery app API:  
  
    - Login API and page
  
    - Registration API and page
  
    - Restaurant search API and page
    
___
### List of features:
1. User create.
2. Token base authentication.
3. Password reset.
4. Logout.
5. Restaurant list
6. Search Restaurant.
7. Full feature web UI.

## APIs List
1. ***Create user endpoint*** : http://127.0.0.1:8000/api/user/create/
```python
HTTP 201 Created
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "email": "rakib@gmail.com",
    "name": "Rakib"
}
```
2. ***Generate token endpoint*** : http://127.0.0.1:8000/api/user/token/
```python
HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "token": "ac1d9998d2d15b674812729f7484d2f720344c6c"
}
```
3. ***Password reset endpoint*** : http://127.0.0.1:8000/api/user/password-reset/
```python
HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "Success": "We have sent you a link to reset your password"
}
```
4. ***Logout endpoint*** : http://127.0.0.1:8000/api/user/logout/
```python
HTTP 204 No Content
Allow: GET, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

"Success"
```
5. ***News api endpoint*** : http://127.0.0.1:8000/api/restaurant-list/
```python
HTTP 200 OK
Allow: GET, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept
{
    "result": [
        'list of restaurant'
    ]
}
```
6. For invalid token:
```python
HTTP 401 Unauthorized
Allow: GET, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept
WWW-Authenticate: Token

{
    "detail": "Invalid token."
}
```
## Web endpoints:
1. User login: http://127.0.0.1:8000/login/
2. User registration: http://127.0.0.1:8000/login/
3. Password reset: http://127.0.0.1:8000/password-reset/
4. Restaurant list: http://127.0.0.1:8000/


## Setup Process
> ❗Make sure You have python3 and pip installed on your machine.

### Step 1

1. Create a folder where you want to clone the project.
   - I am creating a folder named ‘example’ in desktop

2. Now navigate to "example" via cmd or terminal

(Linux)

```bash
cd desktop/example
```

### Step 2

> It️ *Optional but better to use a virtual environment for every project.*

If don’t have any virtual environment manager installed in your machine.


1. Now clone the project and navigate to food-delivery

```bash
git clone https://gitlab.com/rakibulislam01/food-delivery.git
cd food-delivery
```

2. Install all the dependencies for the project.


```bash
pip install -r requirements.txt
```

### Step 3

1. Fast You need to create two free accounts in Sendgrid beforehand.
   - Sendgrid ( https://sendgrid.com/ ) - For sending emails

1. Create a .env file in the project directory where ```setting.py``` file located, 
   open it with your favorite text editor and paste the bellow lines

>❗I share the key for testing purposes. It will be deleted soon.
```.env
SECRET_KEY = *@jqu&dhs+q%4n%g1dpuc61flcy7zcny)%d(x+pc21)_$dx

DEBUG = True

ALLOWED_HOSTS=.localhost, 127.0.0.1

EMAIL_HOST = smtp.sendgrid.net
EMAIL_HOST_USER = apikey
EMAIL_HOST_PASSWORD = 'Your sendgrid account password'

FORM_EMAIL = allinoner7@gmail.com
```

2. You are all setup, let’s migrate now.

```bash
python manage.py makemigrations
python manage.py migrate
```

3. Create a superuser to rule the site 😎 [email user]

```bash
python manage.py createsuperuser
python manage.py runserver
```

> *follow the instructions*

* After creating superuser. Then login to **Admin** user [http://127.0.0.1:8000/admin/]. 

* Then create restaurant. 
  
* After create restaurant then logout form admin and Register a new user.

* And login as new user. 

4. Hahh! Long wait. Let’s visit the site now


Visit [http://127.0.0.1:8000/](http://127.0.0.1:8000/) and rock 🤘
After all okay, Then you need to registration for new user.
---
